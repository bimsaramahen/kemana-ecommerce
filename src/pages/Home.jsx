import React, { useState } from 'react';
import '../App.css';
import JsonData from '../Items.json';
import Card from '../components/card';
import Navbar from '../components/navbar';

function Home(props) {

    /*Fetching and Mapping The Json Data*/
const DisplayData=JsonData.map(
    (info)=>{
        return(
            /*Calling Reusable Card Component with Props*/
            <Card id = {info.id} name = {info.name} price = {info.price} image = {info.image} quantity = {info.quantity} cartItems = {props.cartItems} setCartItems = {props.setCartItems}/>
        )
    }
)

    return (
        /*Rendering the Home Screen*/
        <div>
            <Navbar cartItems = {props.cartItems}/>
            <div className='home'>
                <div className='container'>
                    {DisplayData}
                </div>
            </div>
        </div>
      
    );
  }
  
  export default Home;