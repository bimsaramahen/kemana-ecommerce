import React, { useState } from 'react';
import '../App.css';
import { useNavigate } from "react-router-dom";

const Success =()=>{

    /*Route Redirect*/
    let navigate = useNavigate(); 
    const routeChange = () =>{ 
    let path = `/`; 
    navigate(path);
    localStorage.removeItem('cart');
    window.location.reload(false);
    }

    return(
        <div className='success'>
            <h1>Payment Successful!</h1>
            <img src="/img/success.jpg" alt="image"/>
            <p>Your Items will be Delivered Shortly!</p>
            <button onClick={routeChange}><span>Shop Some More</span></button>
        </div>
    );
}

export default Success;