import React, {useState, useEffect} from 'react';
import '../App.css';
import { useNavigate } from "react-router-dom";
import Bill from '../components/bill';

function Cart(props){

    /*Route Redirect*/
    let navigate = useNavigate(); 
    const routeChange = () =>{ 
    let path = `/`; 
    navigate(path);
}
    /*Declaring State and Non State Variables for Prices and Quantity Calculations*/
    let initialQuantity = 0;
    props.cartItems.slice(1).map(
        (data) => {
            initialQuantity = initialQuantity+1;
        }
    )
    let initialPrice = 0;
    let initialTaxedPrice = 0;
    props.cartItems.slice(1).map(
        (data) => {
            initialPrice = initialPrice+parseInt(data.price);
            initialTaxedPrice = initialTaxedPrice+(parseInt(data.price)+(parseInt(data.price)*1.23/100));
        }
    )

    const [totalPrice, setTotalPrice] = useState(initialPrice);
    const [taxedPrice, setTaxedPrice] = useState(initialTaxedPrice);
    const [totalQuantity, setTotalQuantity] = useState(initialQuantity);
    let calc = 0;
    let calcTax = 0;
    let calcQ = 1;

    /*state array for quantity*/
    const [quantities, setQuantities] = useState([
        {id: 1, quantity:1},
        {id: 2, quantity:1},
        {id: 3, quantity:1},
        {id: 4, quantity:1},
        {id: 5, quantity:1},
        {id: 6, quantity:1},
        {id: 7, quantity:1},
        {id: 8, quantity:1},
        {id: 9, quantity:1},
        {id: 10, quantity:1}]);

    /*array for prices*/
    const initialPrices = [];
    props.cartItems.slice(1).map(
        (data) => {
            initialPrices.push({id: data.id, price: parseInt(data.price)});
        }
    )
    const [prices,setPrices] = useState(initialPrices);

    /*Fetching from LocalStorage*/
    const displayFields = props.cartItems.slice(1).map(
        (info) => {

            /*Total Price and Taxed Price Calculations*/
            
            const UpdatePrice =()=>{

                const newPrices = prices.map(obj => {
                    if (obj.id === info.id) {
                      return {...obj, price: info.price*found.quantity};
                    }
                    return obj;
                  });
                setPrices(newPrices);
                calculatePrice();
            }

            const calculatePrice =()=>{
                prices.map(
                    (x) => {
                        calc = calc+x.price;
                    }
                )
                setTotalPrice(calc);

                prices.map(
                    (y) => {
                        calcTax = calcTax+(y.price+(y.price*(1.23/100)));
                    }
                )
                setTaxedPrice(calcTax);
                calcQuantity();
            }

            /*Incrementing and Decrementing Quantity*/
            const Increment = () =>{
                const newQuantity = quantities.map(obj => {
                    if (obj.id === info.id) {
                      return {...obj, quantity: obj.quantity+1};
                    }
                    return obj;
                  });
                setQuantities(newQuantity);
                UpdatePrice();
            }
            const decrement = () =>{
                const newQuantity = quantities.map(obj => {
                    if (obj.id === info.id) {
                        if(obj.quantity != 0){
                            return {...obj, quantity: obj.quantity-1};
                        }
                    }
                    return obj;
                  });
                setQuantities(newQuantity);
                UpdatePrice();
            }

            /*Finding an Object in Quantity Array*/
            const found = quantities.find(obj => {
                return obj.id === info.id;
              });

            /*Removing an Item in Cart*/
            const removeItem =()=>{
                props.setCartItems((current) =>
                current.filter((x) => x.id !== info.id)
                );
                const newQuantity = quantities.map(obj => {
                    if (obj.id === info.id) {
                        return {...obj, quantity: 0};
                    }
                    return obj;
                  });
                setQuantities(newQuantity);
                UpdatePrice();

            };
        

            return(
                <tr>
                    <td className='tdleft'>{info.name}</td>
                    <td>$ {info.price}</td>
                    <td className='tdcenter'><button onClick={() => { decrement()}}>-</button> {found.quantity} <button onClick={() => {Increment()}}>+</button></td>
                    <td className='tdcenter'><button className='remove' onClick={() => { removeItem()}}>X</button></td>
                </tr>
            )
        }
    )

    /*Clearing The Cart*/
    const removeCart =()=>{
        localStorage.removeItem('cart');
        window.location.reload(false);
    }

    /*Calculating Total Quantity*/
    const calcQuantity =()=>{
        quantities.map(
            (q) => {
                const found = props.cartItems.some(el => el.id === q.id);
                if(found) {
                    calcQ = calcQ+q.quantity;
                }
            }
        )
        setTotalQuantity(calcQ);
        console.log(prices);
    }

    /*Rendering the Cart*/
    return(
        <div className='CartContainer'>
            <button className="homeButton" onClick={routeChange}>Back to Home</button>
            {props.cartItems.length === 1 && <div className='empty'>
                <img src="/img/empty.jpg" alt="image"/>
                <h1>?</h1>
                <h2>Oh No!</h2>
                <p>Your Cart appears to be Empty.</p>
                <p>Go Back and Add Some Items!</p>
            </div>}

            <div className='Cart'>
                <table>
                 <tr>
                        <th>Item</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Remove Item</th>
                    </tr>
                  {displayFields}
                    <tr>
                        {/*Totals*/}
                        <td className='tdleft'>Total:</td>
                       <td>$ {totalPrice.toFixed(2)}</td>
                       <td className='tdcenter'>{totalQuantity}</td>
                        <td><button className='removeall' onClick={removeCart}>Clear Cart</button></td>
                 </tr>
              </table>
            </div>
            {/*Calling the Bill Component*/}
            {props.cartItems.length !== 1 && <Bill totalPrice={totalPrice} taxedPrice={taxedPrice}/>}
        </div>
    );
}

export default Cart;