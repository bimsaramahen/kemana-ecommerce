import React, {useState, useEffect} from 'react';
import '../App.css';

const Card = (props)=>{

    /*Adding to Cart (Local Storage)*/
    const addToCart = (props) =>{
        /*Checking if Item is Already in Cart*/
        const found = props.cartItems.some(el => el.id === props.id);
        if(!found) {
            props.setCartItems([...props.cartItems, props]);
        } else {
            alert('You Already Added This Item');
        }
    }
    
    return(
        /*Card Component*/
        <div className='card'>
            <img src={props.image}/>
            <h3>{props.name} - ${props.price}</h3>
            <button onClick={()=>addToCart(props)}>Add to Cart</button>
        </div>
    );
}

export default Card;