import React, {useState, useEffect} from 'react';
import '../App.css';
import { useNavigate } from "react-router-dom";

const Bill =(props)=>{

    /*Route Redirect*/
    let navigate = useNavigate(); 
    const routeChange = () =>{ 
    let path = `/Success`; 
    navigate(path);
    }

    /*Total Price*/
    let totalPay = props.taxedPrice + 500;

    return(

        /*Rendering the Bill*/
        <div className = 'Bill'>
            <h1>This is Your Bill</h1>
            <h4>Price of Purchased Items:</h4> <span>${props.totalPrice.toFixed(2)}</span>
            <h4>Price With Tax:</h4><span>${props.taxedPrice.toFixed(2)}</span>
            <h4>Shipping:</h4><span>$500.00</span>
            <hr/>
            <h4>Your Total Payment:</h4><span>${totalPay.toFixed(2)}</span>
            
            <button className='checkout' onClick={routeChange}>Pay Now</button>
        </div>
    )
}

export default Bill;