import React, { useState } from 'react';
import '../App.css';
import { useNavigate } from "react-router-dom";

const Navbar = (props) =>{
    /*Route Redirect*/
    let navigate = useNavigate(); 
    const routeChange = () =>{ 
    let path = `/Cart`; 
    navigate(path);
}

    return (
        <div className='topdiv'>
                <h1 className='logo'>Ecommerce App</h1>
                <div className='cartSet'>

                    {/*Counter for Items in Cart*/}
                    <p className='counter'>{parseInt(props.cartItems.length)-1}</p>

                    {/*Button to Open Cart*/}
                    <button className='cartbutton' onClick={routeChange}>View Cart</button>
                </div>
        </div>
    )
    
};

export default Navbar;