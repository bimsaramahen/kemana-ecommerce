import React, { useState, useEffect } from 'react';
import './App.css';
import Home from "./pages/Home";
import Cart from "./pages/Cart";
import Success from "./pages/success";
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

function App() {

  const cartFromLocalStorage = JSON.parse(localStorage.getItem('cart') || '[]');

    const [cartItems, setCartItems] = useState([cartFromLocalStorage]);

    useEffect(() => {
        localStorage.setItem('cart', JSON.stringify(cartItems));
      }, [cartItems]);

  return (
        <Router>
          <Routes>
            <Route exact path='/' element={<Home cartItems = {cartItems} setCartItems = {setCartItems}/>} />
            <Route exact path='/cart' element={<Cart cartItems = {cartItems} setCartItems = {setCartItems}/>} />
            <Route exact path='/success' element={<Success/>} />
            </Routes>
        </Router> 
  );
}

export default App;
